package plugIn.serverCommunication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import org.apache.solr.search.SolrIndexSearcher;

public class ServerCommunication {
	final static String USER_AGENT = "Mozilla/5.0";
	
	
	private String rootUrl ;
	private String idFieldName;
	
	public ServerCommunication(int port, String idFieldName) {
		rootUrl = "http://localhost:"+port+"/";
		this.idFieldName = idFieldName;
	}

	public ServerCommunication(int port) {
		rootUrl = "http://localhost:"+port+"/";
		this.idFieldName = null;
	}
	
	public String initializeWeightVector(List<Float> initialWeights) throws Exception {
		
		String url = rootUrl + "receivewv/?wv=" + floatListToString(initialWeights) ;
		
		return serverRequest(url);
		
	}
	
	private String floatListToString(List<Float> list) {	//TODO: convert to generic type
		StringBuilder sb = new StringBuilder();
		String del = "";
		for (Float element : list) {
			sb.append(del+element);
			del = ",";
		}
		return sb.toString();
	}

	public  String searchRequest(String qid, int offset) throws Exception {
		String url = rootUrl + "searchrequest/?qid=" + qid 
				+ "&offset="
				+offset
				;
		return serverRequest(url);
	}
	
	public  String postLists(String qid, int[] interleavedList, int[] assignments, 
									String fq,
									int[] originalList1, int[] originalList2,
									SolrIndexSearcher searcher) throws Exception {
		
		String[] schemaDocIdsInterleavedList = getSchemaDocIds(interleavedList, searcher);
		String[] l1 = getSchemaDocIds(originalList1, searcher);
		String[] l2 = getSchemaDocIds(originalList2, searcher);
		
		
		String url = rootUrl + "postlists/?"
						+ "qid=" + qid
						+ "&il=" + stringListToString(schemaDocIdsInterleavedList)
						+ "&a=" + intListToString(assignments)
						+ "&fq=" + URLEncoder.encode(fq, "UTF-8")
						+ "&l1=" + stringListToString(l1)
						+ "&l2=" + stringListToString(l2)
						; 
		return serverRequest(url);
	}
	
	private  String stringListToString(
			String[] list) {
		StringBuilder sb = new StringBuilder();
		String del = "";
		for (String element : list) {
			sb.append(del+element);
			del = ",";
		}
		return sb.toString();
	}

	/**
	 * converts lucene doc id's to actual id's as defined by the schema "id" field
	 * @param docList
	 * @param searcher
	 * @return
	 * @throws IOException
	 */
	private  String[] getSchemaDocIds(
			int[] docList, SolrIndexSearcher searcher) throws IOException {
		String[] l = new String[docList.length];
		for (int i = 0 ; i < docList.length ;i++)
			l[i] = searcher.doc(docList[i]).get(idFieldName);
		return l;
	}

	public  String addClick(String qid, String docid) throws Exception {
		String url = rootUrl + "addclick/?"
				+ "qid=" + qid
				+ "&docid=" + docid;
		return serverRequest(url);
	}
	

	public  String getCurrentWeightVector() throws Exception {
		String url = rootUrl + "currentweightvector";
		return serverRequest(url);
	}
	
	private  String intListToString(int[] list) {
		StringBuilder sb = new StringBuilder();
		String del = "";
		for (int element : list) {
			sb.append(del+element);
			del = ",";
		}
		return sb.toString();
	}

	
	// HTTP GET request
	public  String serverRequest(String url) throws Exception {
 
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		con.setRequestProperty("User-Agent", USER_AGENT);
 
//		int responseCode = con.getResponseCode();
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		return response.toString();
	}


}
