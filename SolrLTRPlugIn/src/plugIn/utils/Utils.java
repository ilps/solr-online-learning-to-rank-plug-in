package plugIn.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class Utils {
	public static <K,V extends Comparable<? super V>> SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
        SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
            new Comparator<Map.Entry<K,V>>() {
                @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                    int res = e1.getValue().compareTo(e2.getValue());
                    return res != 0 ? res : 1; // Special fix to preserve items with equal values
                }
            }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

	public static double[] stringToDoubleList(String weightVector) throws NumberFormatException{
		
		String[] wv = weightVector.substring(1, weightVector.length()-1).trim().split("\\s+|\\t+");	//TODO: check it
		double[] res = new double[wv.length];
		for (int i = 0; i < wv.length; i++)
			res[i] = Double.parseDouble(wv[i]);
		return res;
	}
}
