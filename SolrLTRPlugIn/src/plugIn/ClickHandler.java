package plugIn;


import org.apache.solr.common.SolrException;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.solr.handler.RequestHandlerBase;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;

import plugIn.serverCommunication.ServerCommunication;

public class ClickHandler extends RequestHandlerBase {
	
	volatile long numRequests = 0;
	volatile long numErrors = 0;
	//volatile String lastQueryId = ""; //TODO: remove this
	 private ServerCommunication serverCommunication;

	 @Override
	  public void init( NamedList params )
	  {
		  int externalServerPort = Integer.parseInt((String)params.get("externalServerPort"));
		  serverCommunication = new ServerCommunication(externalServerPort);
	  }
	
	
	@Override
	public String getDescription() {
		return "Online LTR plugin click Handler";
	}

	@Override
	public NamedList<Object> getStatistics()  {
		NamedList<Object> lst = new SimpleOrderedMap<Object>();
		lst.add("Click requests", numRequests);
		lst.add("Click misses", numErrors);	
		try {
			lst.add("Current weight vector", serverCommunication.getCurrentWeightVector());
		} catch (Exception e) {
			throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Problem with external server when receiving the weight vector");
		}
		return lst;
	}

	@Override
	public void handleRequestBody(SolrQueryRequest req, SolrQueryResponse rsp)
			throws Exception {
		numRequests++;
		
		SolrParams parameters = req.getParams();
		
		String queryId = parameters.get("qid");
		String docId = parameters.get("docid");
		
		if (queryId == null || docId == null) {
			numErrors++;
			throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "need to specify qid and docid in the request");
		}

		String status = serverCommunication.addClick(queryId, docId);
		rsp.add("status", status);
	}



	@Override
	public String getSource() {
		return "https://bitbucket.org/ilps/solr-online-learning-to-rank-plug-in";
	}
	
	@Override
	public String getVersion() {
		return "1.0";
	}
	

}
