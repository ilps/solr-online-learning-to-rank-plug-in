package plugIn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeMap;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.grouping.SearchGroup;
import org.apache.lucene.util.BytesRef;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.GroupParams;
import org.apache.solr.common.params.MoreLikeThisParams;
import org.apache.solr.common.params.ShardParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.solr.common.util.StrUtils;
import org.apache.solr.handler.component.QueryComponent;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.ResultContext;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.schema.IndexSchema;
import org.apache.solr.schema.SchemaField;
import org.apache.solr.search.DocIterator;
import org.apache.solr.search.DocList;
import org.apache.solr.search.DocListAndSet;
import org.apache.solr.search.DocSlice;
import org.apache.solr.search.ExtendedDismaxQParser;
import org.apache.solr.search.Grouping;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;
import org.apache.solr.search.QueryParsing;
import org.apache.solr.search.ReturnFields;
import org.apache.solr.search.SolrIndexSearcher;
import org.apache.solr.search.SolrIndexSearcher.QueryResult;
import org.apache.solr.search.SolrReturnFields;
import org.apache.solr.search.SyntaxError;
import org.apache.solr.search.grouping.CommandHandler;
import org.apache.solr.search.grouping.GroupingSpecification;
import org.apache.solr.search.grouping.distributed.command.QueryCommand;
import org.apache.solr.search.grouping.distributed.command.SearchGroupsFieldCommand;
import org.apache.solr.search.grouping.distributed.command.TopGroupsFieldCommand;
import org.apache.solr.search.grouping.distributed.requestfactory.TopGroupsShardRequestFactory;
import org.apache.solr.search.grouping.distributed.shardresultserializer.SearchGroupsResultTransformer;
import org.apache.solr.search.grouping.distributed.shardresultserializer.TopGroupsResultTransformer;

import plugIn.interleaver.Interleaver;
import plugIn.interleaver.Interleaver.Result;
import plugIn.interleaver.TeamDraftInterleaver;
import plugIn.serverCommunication.ServerCommunication;
import plugIn.utils.Utils;

/**
 * Modified SearchComponent of Apache Solr 
 * @author nickvosk
 *
 */
public class LearningQueryComponent extends QueryComponent {
	
	  private Map<String, Float> numericFeatures ;
	  private Map<String, Float> termFeatures; 
	  private Map<String, Float> phraseFeatures ;
	  private int internalResultLength ;
	  private Interleaver interleaver = new TeamDraftInterleaver();
	  private int defaultRows ;
	  private double[] lastSavedWeightVector = null;
	  private ServerCommunication serverCommunication;
	  
	  @Override
	  public void init( NamedList params )
	  {
		  int externalServerPort = Integer.parseInt((String)params.get("externalServerPort"));
		  String idFieldName = (String) params.get("idFieldName");
		  serverCommunication = new ServerCommunication(externalServerPort, idFieldName);
		  
		  List<Float> initialWeights = new ArrayList<Float>();
		  
		  float textSimilarityWeight = 1.0f;
		  if (params.get("textSimilarityWeight") != null)
			  textSimilarityWeight = (Float) params.get("textSimilarityWeight");
		  
		  initialWeights.add(textSimilarityWeight);
		  
		  numericFeatures = loadWeightedFeatures(params, "numericFeatures");
		  initialWeights.addAll(numericFeatures.values());
			  
		  termFeatures = loadWeightedFeatures(params, "termFeatures");
		  initialWeights.addAll(termFeatures.values());
		  
		  phraseFeatures = loadWeightedFeatures(params, "phraseFeatures");
		  initialWeights.addAll(phraseFeatures.values());
		  
		  //learning only one feature does not make sense
		  if (initialWeights.size() == 1) 
			  throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "You need to provide at least two features to be learned");
		  
		  internalResultLength = Integer.parseInt((String)params.get("internalResultLength"));
		  defaultRows = Integer.parseInt((String)params.get("defaultRows"));

		  try {
			  ////to be used when we search for the first time. Priority given to the saved t.wv of the external server
			serverCommunication.initializeWeightVector(initialWeights);
		} catch (Exception e) {
			throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Problem with external server when initializing the weight vector");
		}
		  try {
			  //synchronize the current weight vector. 
			lastSavedWeightVector = Utils.stringToDoubleList(serverCommunication.getCurrentWeightVector());
		} catch (Exception e) {
			throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Problem with external server when initializing the weight vector");
		}
		  
	  }
	  
	  

	  private Map<String, Float> loadWeightedFeatures(NamedList params, String featuresName) {
		  Map <String, Float> map =   new TreeMap<String, Float> ();
		  if (params.get(featuresName) == null) //empty list
			  return map;
		  NamedList namedList = (NamedList) params.get(featuresName);
		  
		  Iterator<Map.Entry<String,Float>> iterator = namedList.iterator();
		  while (iterator.hasNext()) {
			  Map.Entry<String,Float> entry = iterator.next();
			  map.put(entry.getKey(), entry.getValue()); 
		  }		
		  return map;
	  }



	@Override
	  public void prepare(ResponseBuilder rb) throws IOException
	  {

	    SolrQueryRequest req = rb.req;
	    SolrParams params = req.getParams();
	    if (!params.getBool(COMPONENT_NAME, true)) {
	      return;
	    }
	    SolrQueryResponse rsp = rb.rsp;

	    // Set field flags    
	    ReturnFields returnFields = new SolrReturnFields( req );
	    rsp.setReturnFields( returnFields );
	    int flags = 0;
	    //if (returnFields.wantsScore()) {
	      flags |= SolrIndexSearcher.GET_SCORES;		//TODO: always require scores!
	    //}
	    rb.setFieldFlags( flags );

	    String defType = params.get(QueryParsing.DEFTYPE,QParserPlugin.DEFAULT_QTYPE);

	    // get it from the response builder to give a different component a chance
	    // to set it.
	    String queryString = rb.getQueryString();
	    if (queryString == null) {
	      // this is the normal way it's set.
	      queryString = params.get( CommonParams.Q );
	      rb.setQueryString(queryString);
	    }

	    try {
	      QParser parser = QParser.getParser(rb.getQueryString(), defType, req);
	      Query q = parser.getQuery();
	      if (q == null) {
	        // normalize a null query to a query that matches nothing
	        q = new BooleanQuery();        
	      }

	      rb.setQuery( q );
	      rb.setSortSpec( parser.getSort(true) );
	      rb.setQparser(parser);
	      rb.setScoreDoc(parser.getPaging());
	      
	      String[] fqs = req.getParams().getParams(CommonParams.FQ);
	      if (fqs!=null && fqs.length!=0) {
	        List<Query> filters = rb.getFilters();
	        if (filters==null) {
	          filters = new ArrayList<Query>(fqs.length);
	        }
	        for (String fq : fqs) {
	          if (fq != null && fq.trim().length()!=0) {
	            QParser fqp = QParser.getParser(fq, null, req);
	            filters.add(fqp.getQuery());
	          }
	        }
	        // only set the filters if they are not empty otherwise
	        // fq=&someotherParam= will trigger all docs filter for every request 
	        // if filter cache is disabled
	        if (!filters.isEmpty()) {
	          rb.setFilters( filters );
	        }
	      }
	    } catch (SyntaxError e) {
	      throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, e);
	    }

	    boolean grouping = params.getBool(GroupParams.GROUP, false);
	    if (!grouping) {
	      return;
	    }

	    SolrIndexSearcher.QueryCommand cmd = rb.getQueryCommand();
	    SolrIndexSearcher searcher = rb.req.getSearcher();
	    GroupingSpecification groupingSpec = new GroupingSpecification();
	    rb.setGroupingSpec(groupingSpec);

	    //TODO: move weighting of sort
	    Sort groupSort = searcher.weightSort(cmd.getSort());
	    if (groupSort == null) {
	      groupSort = Sort.RELEVANCE;
	    }

	    // groupSort defaults to sort
	    String groupSortStr = params.get(GroupParams.GROUP_SORT);
	    //TODO: move weighting of sort
	    Sort sortWithinGroup = groupSortStr == null ?  groupSort : searcher.weightSort(QueryParsing.parseSort(groupSortStr, req));
	    if (sortWithinGroup == null) {
	      sortWithinGroup = Sort.RELEVANCE;
	    }

	    groupingSpec.setSortWithinGroup(sortWithinGroup);
	    groupingSpec.setGroupSort(groupSort);

	    String formatStr = params.get(GroupParams.GROUP_FORMAT, Grouping.Format.grouped.name());
	    Grouping.Format responseFormat;
	    try {
	       responseFormat = Grouping.Format.valueOf(formatStr);
	    } catch (IllegalArgumentException e) {
	      throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, String.format(Locale.ROOT, "Illegal %s parameter", GroupParams.GROUP_FORMAT));
	    }
	    groupingSpec.setResponseFormat(responseFormat);

	    groupingSpec.setFields(params.getParams(GroupParams.GROUP_FIELD));
	    groupingSpec.setQueries(params.getParams(GroupParams.GROUP_QUERY));
	    groupingSpec.setFunctions(params.getParams(GroupParams.GROUP_FUNC));
	    groupingSpec.setGroupOffset(params.getInt(GroupParams.GROUP_OFFSET, 0));
	    groupingSpec.setGroupLimit(params.getInt(GroupParams.GROUP_LIMIT, 1));
	    groupingSpec.setOffset(rb.getSortSpec().getOffset());
	    groupingSpec.setLimit(rb.getSortSpec().getCount());
	    groupingSpec.setIncludeGroupCount(params.getBool(GroupParams.GROUP_TOTAL_COUNT, false));
	    groupingSpec.setMain(params.getBool(GroupParams.GROUP_MAIN, false));
	    groupingSpec.setNeedScore((cmd.getFlags() & SolrIndexSearcher.GET_SCORES) != 0);
	    groupingSpec.setTruncateGroups(params.getBool(GroupParams.GROUP_TRUNCATE, false));
	  }
	  
	  /**
	   * Actually run the query
	   */
	  @Override
	  public void process(ResponseBuilder rb) throws IOException
	  {
		 
	    SolrQueryRequest req = rb.req;
	    SolrQueryResponse rsp = rb.rsp;
	   
	    
	    SolrParams params = req.getParams();
	    if (!params.getBool(COMPONENT_NAME, true)) {
	      return;
	    }
	    SolrIndexSearcher searcher = req.getSearcher();

	    if (rb.getQueryCommand().getOffset() < 0) {
	      throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "'start' parameter cannot be negative");
	    }

	    // -1 as flag if not set.
	    long timeAllowed = (long)params.getInt( CommonParams.TIME_ALLOWED, -1 );

	    // Optional: This could also be implemented by the top-level searcher sending
	    // a filter that lists the ids... that would be transparent to
	    // the request handler, but would be more expensive (and would preserve score
	    // too if desired).
	    String ids = params.get(ShardParams.IDS);
	    if (ids != null) {
	      SchemaField idField = req.getSchema().getUniqueKeyField();
	      List<String> idArr = StrUtils.splitSmart(ids, ",", true);
	      int[] luceneIds = new int[idArr.size()];
	      int docs = 0;
	      for (int i=0; i<idArr.size(); i++) {
	        int id = req.getSearcher().getFirstMatch(
	                new Term(idField.getName(), idField.getType().toInternal(idArr.get(i))));
	        if (id >= 0)
	          luceneIds[docs++] = id;
	      }

	      DocListAndSet res = new DocListAndSet();
	      res.docList = new DocSlice(0, docs, luceneIds, null, docs, 0);
	      if (rb.isNeedDocSet()) {
	        //  create a cache for this!
	        List<Query> queries = new ArrayList<Query>();
	        queries.add(rb.getQuery());
	        List<Query> filters = rb.getFilters();
	        if (filters != null) queries.addAll(filters);
	        res.docSet = searcher.getDocSet(queries);
	      }
	      rb.setResults(res);
	      
	      ResultContext ctx = new ResultContext();
	      ctx.docs = rb.getResults().docList;
	      ctx.query = null; // anything?
	      rsp.add("response", ctx);
	      return;
	    }
	    SolrIndexSearcher.QueryCommand cmd = rb.getQueryCommand();

	    cmd.setTimeAllowed(timeAllowed);
	    SolrIndexSearcher.QueryResult result = new SolrIndexSearcher.QueryResult();

	    //
	    // grouping / field collapsing
	    //
	    GroupingSpecification groupingSpec = rb.getGroupingSpec();
	    if (groupingSpec != null) {
	      try {
	        boolean needScores = (cmd.getFlags() & SolrIndexSearcher.GET_SCORES) != 0;
	        if (params.getBool(GroupParams.GROUP_DISTRIBUTED_FIRST, false)) {
	          CommandHandler.Builder topsGroupsActionBuilder = new CommandHandler.Builder()
	              .setQueryCommand(cmd)
	              .setNeedDocSet(false) // Order matters here
	              .setIncludeHitCount(true)
	              .setSearcher(searcher);

	          for (String field : groupingSpec.getFields()) {
	            topsGroupsActionBuilder.addCommandField(new SearchGroupsFieldCommand.Builder()
	                .setField(searcher.getSchema().getField(field))
	                .setGroupSort(groupingSpec.getGroupSort())
	                .setTopNGroups(cmd.getOffset() + cmd.getLen())
	                .setIncludeGroupCount(groupingSpec.isIncludeGroupCount())
	                .build()
	            );
	          }

	          CommandHandler commandHandler = topsGroupsActionBuilder.build();
	          commandHandler.execute();
	          SearchGroupsResultTransformer serializer = new SearchGroupsResultTransformer(searcher);
	          rsp.add("firstPhase", commandHandler.processResult(result, serializer));
	          rsp.add("totalHitCount", commandHandler.getTotalHitCount());
	          rb.setResult(result);
	          return;
	        } else if (params.getBool(GroupParams.GROUP_DISTRIBUTED_SECOND, false)) {
	          CommandHandler.Builder secondPhaseBuilder = new CommandHandler.Builder()
	              .setQueryCommand(cmd)
	              .setTruncateGroups(groupingSpec.isTruncateGroups() && groupingSpec.getFields().length > 0)
	              .setSearcher(searcher);

	          for (String field : groupingSpec.getFields()) {
	            String[] topGroupsParam = params.getParams(GroupParams.GROUP_DISTRIBUTED_TOPGROUPS_PREFIX + field);
	            if (topGroupsParam == null) {
	              topGroupsParam = new String[0];
	            }

	            List<SearchGroup<BytesRef>> topGroups = new ArrayList<SearchGroup<BytesRef>>(topGroupsParam.length);
	            for (String topGroup : topGroupsParam) {
	              SearchGroup<BytesRef> searchGroup = new SearchGroup<BytesRef>();
	              if (!topGroup.equals(TopGroupsShardRequestFactory.GROUP_NULL_VALUE)) {
	                searchGroup.groupValue = new BytesRef(searcher.getSchema().getField(field).getType().readableToIndexed(topGroup));
	              }
	              topGroups.add(searchGroup);
	            }

	            secondPhaseBuilder.addCommandField(
	                new TopGroupsFieldCommand.Builder()
	                    .setField(searcher.getSchema().getField(field))
	                    .setGroupSort(groupingSpec.getGroupSort())
	                    .setSortWithinGroup(groupingSpec.getSortWithinGroup())
	                    .setFirstPhaseGroups(topGroups)
	                    .setMaxDocPerGroup(groupingSpec.getGroupOffset() + groupingSpec.getGroupLimit())
	                    .setNeedScores(needScores)
	                    .setNeedMaxScore(needScores)
	                    .build()
	            );
	          }

	          for (String query : groupingSpec.getQueries()) {
	            secondPhaseBuilder.addCommandField(new QueryCommand.Builder()
	                .setDocsToCollect(groupingSpec.getOffset() + groupingSpec.getLimit())
	                .setSort(groupingSpec.getGroupSort())
	                .setQuery(query, rb.req)
	                .setDocSet(searcher)
	                .build()
	            );
	          }

	          CommandHandler commandHandler = secondPhaseBuilder.build();
	          commandHandler.execute();
	          TopGroupsResultTransformer serializer = new TopGroupsResultTransformer(rb);
	          rsp.add("secondPhase", commandHandler.processResult(result, serializer));
	          rb.setResult(result);
	          return;
	        }

	        int maxDocsPercentageToCache = params.getInt(GroupParams.GROUP_CACHE_PERCENTAGE, 0);
	        boolean cacheSecondPassSearch = maxDocsPercentageToCache >= 1 && maxDocsPercentageToCache <= 100;
	        Grouping.TotalCount defaultTotalCount = groupingSpec.isIncludeGroupCount() ?
	            Grouping.TotalCount.grouped : Grouping.TotalCount.ungrouped;
	        int limitDefault = cmd.getLen(); // this is normally from "rows"
	        Grouping grouping =
	            new Grouping(searcher, result, cmd, cacheSecondPassSearch, maxDocsPercentageToCache, groupingSpec.isMain());
	        grouping.setSort(groupingSpec.getGroupSort())
	            .setGroupSort(groupingSpec.getSortWithinGroup())
	            .setDefaultFormat(groupingSpec.getResponseFormat())
	            .setLimitDefault(limitDefault)
	            .setDefaultTotalCount(defaultTotalCount)
	            .setDocsPerGroupDefault(groupingSpec.getGroupLimit())
	            .setGroupOffsetDefault(groupingSpec.getGroupOffset())
	            .setGetGroupedDocSet(groupingSpec.isTruncateGroups());

	        if (groupingSpec.getFields() != null) {
	          for (String field : groupingSpec.getFields()) {
	            grouping.addFieldCommand(field, rb.req);
	          }
	        }

	        if (groupingSpec.getFunctions() != null) {
	          for (String groupByStr : groupingSpec.getFunctions()) {
	            grouping.addFunctionCommand(groupByStr, rb.req);
	          }
	        }

	        if (groupingSpec.getQueries() != null) {
	          for (String groupByStr : groupingSpec.getQueries()) {
	            grouping.addQueryCommand(groupByStr, rb.req);
	          }
	        }

	        if (rb.doHighlights || rb.isDebug() || params.getBool(MoreLikeThisParams.MLT, false)) {
	          // we need a single list of the returned docs
	          cmd.setFlags(SolrIndexSearcher.GET_DOCLIST);
	        }

	        grouping.execute();
	        if (grouping.isSignalCacheWarning()) {
	          rsp.add(
	              "cacheWarning",
	              String.format(Locale.ROOT, "Cache limit of %d percent relative to maxdoc has exceeded. Please increase cache size or disable caching.", maxDocsPercentageToCache)
	          );
	        }
	        rb.setResult(result);

	        if (grouping.mainResult != null) {
	          ResultContext ctx = new ResultContext();
	          ctx.docs = grouping.mainResult;
	          ctx.query = null; // add the query?
	          rsp.add("response", ctx);
	          rsp.getToLog().add("hits", grouping.mainResult.matches());
	        } else if (!grouping.getCommands().isEmpty()) { // Can never be empty since grouping.execute() checks for this.
	          rsp.add("grouped", result.groupedResults);
	          rsp.getToLog().add("hits", grouping.getCommands().get(0).getMatches());
	        }
	        return;
	      } catch (SyntaxError e) {
	        throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, e);
	      }
	    }

	    
	    //if it is not a sorting query, use the learning component
	    if (cmd.getSort() == null) {
	    
		    /*****
		     * PLUG-IN code starts here
		     *****/
	        
		    // normal search result
		    
		    String qid = params.get("qid");
	
		    if (qid == null) 
		    	throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "all queries must contain a unique qid");
		    
		    int offset = cmd.getOffset() ;
		    
		    IndexSchema schema = req.getSchema();
		    long currentSecs = System.currentTimeMillis() / 1000;	//to secs
		    
		    cmd.setLen(internalResultLength);
	
		    double [] currentWeightVector = null;
		    double [] candidateWeightVector = null;	
		    		    
		    try {
		    	//communicate with the server, get weight vector(s)
		    	//handles pagination : we don't want a second page query to be an experimental query
		    	String response = serverCommunication.searchRequest(qid, offset);
	
				int pos = response.indexOf(';');		
				//no candidate weight vector returned => not an experiment query
				if (pos == -1 || offset != 0)  
					currentWeightVector = Utils.stringToDoubleList(response);
				//experiment query
				else {
					currentWeightVector = Utils.stringToDoubleList(response.substring(0, pos));
					candidateWeightVector = Utils.stringToDoubleList(response.substring(pos+1));
				}
				//save currentWeightVector in case of external server failure
				cacheWeightVector (currentWeightVector) ;
					
			} catch (Exception e) {
				//if a server error occurs, try to use the saved weight vector. If it fails, return error
				if ((currentWeightVector = tryUseSavedWeightVector()) == null)
					throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Problem with the external server (searchrequest) : "  
									+ e.getClass().getName() + " " + e.getMessage());
			}
	
		    try {
			    //search with current weight vector
			    DocList docList1 = searchAndGetDocList(rb, currentWeightVector, searcher, result, cmd);
			    
			    //if the result list is not empty
			    if (docList1.size() != 0) {
			    
				    Object[] minFeatureScoresCurrentWeightVector = new Object[numericFeatures.size()+1];
				    Object[] maxFeatureScoresCurrentWeightVector = new Object[numericFeatures.size()+1];
			
				    //get min-max values for all features in the documents retrieved
				    getMinMax(searcher, docList1, minFeatureScoresCurrentWeightVector, maxFeatureScoresCurrentWeightVector, schema, currentSecs);
				    
					int[] list1 = searchReranking (docList1, searcher, currentWeightVector, minFeatureScoresCurrentWeightVector, 
												maxFeatureScoresCurrentWeightVector, schema, currentSecs); 
			
					int[] list2 = null;
					
					DocList resultList = null;	//this will be the final result list
				
					//if it is an experiment query, search with candidate weight vector and build interleaved list
					if (isExperimentQuery(candidateWeightVector)) {
						 
						Object[] minFeatureScoresCandidateWeightVector = new Object[numericFeatures.size()+1];
					    Object[] maxFeatureScoresCandidateWeightVector = new Object[numericFeatures.size()+1];
					    
					    DocList docList2 = null;
					    docList2 = searchAndGetDocList(rb, candidateWeightVector, searcher, result, cmd, docList1);
					    
					    //if we use the same list for re-ranking
					    if (docList1 == docList2) {
					    	//also use the previous min-max
					    	minFeatureScoresCandidateWeightVector = minFeatureScoresCurrentWeightVector;
					    	maxFeatureScoresCandidateWeightVector = maxFeatureScoresCurrentWeightVector;
					    }
					    else 
					    	//get min-max values for all features in the documents of the second list 
						    getMinMax(searcher, docList2, minFeatureScoresCandidateWeightVector, maxFeatureScoresCandidateWeightVector, schema, currentSecs);  	
					    
						
						list2 = searchReranking (docList2, searcher, candidateWeightVector, minFeatureScoresCandidateWeightVector, 
								maxFeatureScoresCandidateWeightVector, schema, currentSecs);
		
						//interleave the two lists
						Result interleaverResult = interleaver.interleave(list1, list2, Math.max(list1.length, list2.length));
						
						resultList = buildInterleavedDocList(interleaverResult.interleavedList);
						
						try {
							String fq = cmd.getQuery().toString();
							//send interleavedList and assignments to the server
							serverCommunication.postLists(qid, interleaverResult.interleavedList, interleaverResult.assignments, fq, list1, list2, searcher);
						} catch (Exception e) {
							throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Problem with the external server (postlists) : " + e.getMessage() );
						}
					}
					//not an experiment query, just use the result from the current weight vector
					else {
						resultList = buildSingleDocList(list1);
					
					}			
					//update response result using result list
					result.setDocList(resultList);
					
				    //if it's an experiment query, add qid in response, in order for the clicks to be handled in the front-end
				    if (isExperimentQuery(candidateWeightVector))
				    	rsp.add("qid", qid);
			    }  
		    }
	    
		    catch (SyntaxError e) {
		    	throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Problem with searching with weight vectors : "  
						+ e.getClass().getName() + " " + e.getMessage());
			}
	    }
	    //normal search
	    else {
	    	searcher.search(result,cmd);
	    }
		/*
		 * plug-in code ends here
		 */
	    rb.setResult( result );	
	    
	    ResultContext ctx = new ResultContext();
	    ctx.docs = rb.getResults().docList;
	    
	    //inconsistent query in the response
	    ctx.query = rb.getQuery();
	    rsp.add("response", ctx);
	    
	    
	    rsp.getToLog().add("hits", rb.getResults().docList.matches());
	    doFieldSortValues(rb, searcher);
	    doPrefetch(rb);
	  }
	  
	  private DocList searchAndGetDocList(ResponseBuilder rb,
				double[] weightVector, SolrIndexSearcher searcher,
				QueryResult result,
				org.apache.solr.search.SolrIndexSearcher.QueryCommand cmd ) throws SyntaxError {
		  return searchAndGetDocList(rb, weightVector, searcher, result, cmd, null);
	  }
	  
	 private DocList searchAndGetDocList(ResponseBuilder rb,
			double[] weightVector, SolrIndexSearcher searcher,
			QueryResult result,
			org.apache.solr.search.SolrIndexSearcher.QueryCommand cmd,
			DocList currentWVList) throws SyntaxError {

		 	Query query = null ;
		 
		    //it we are learning term or phrase features
		    if (learnTermOrPhraseFeatures()) {
		    	//create query given current weight vector using field boosting,set to cmd and search
		    	query = constructWeightVectorQuery(rb, weightVector);
		    }
		    //use the list from the current wv, no need for another query as we will just rerank.
		    else if (currentWVList != null) {
		    	return currentWVList;
		    }
		    //get the query from the response builder
		    else {
		    	query = rb.getQuery();
		    }
		    
		    cmd.setLen(internalResultLength);	//for re-ranking
		    cmd.setQuery(query);
		    
 
		    try {
				searcher.search(result,cmd);
			} catch (IOException e) {
				throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Problem with query : " + e);
			}
		    
		    return result.getDocList();	 
	}

	private boolean learnTermOrPhraseFeatures() {
		return !termFeatures.isEmpty() || !phraseFeatures.isEmpty();
	}



	private Query constructWeightVectorQuery(ResponseBuilder rb, double[] weightVector) throws SyntaxError {	    
	    String qfString = constructQfString(weightVector);
	    String pfString = constructPfString(weightVector);
		 
		QParser oldQParser = rb.getQparser();
		//get params of the previous query, so we can change them
		NamedList<Object> newParams = oldQParser.getParams().toNamedList();
		
		if (!qfString.isEmpty())
			updateParams(newParams, "qf", qfString);
		if (!pfString.isEmpty())
			updateParams(newParams, "pf", pfString);
		
		
		ExtendedDismaxQParser newQParser = new ExtendedDismaxQParser(
																	rb.getQueryString(), oldQParser.getLocalParams(), 
																	SolrParams.toSolrParams(newParams), rb.req);
							
		return newQParser.parse();
	}


	private void updateParams(NamedList<Object> params, String parameter, String stringValue) {
		int pos = params.indexOf(parameter, 0);
		if (pos == -1) // parameter does not exist
			params.add(parameter, stringValue);
		else {
			params.setVal(pos, stringValue);

		}
	}

	private String constructPfString(double[] weightVector) {
		double[] phraseFeaturesWeights = getPhraseFeaturesWeights(weightVector);
		StringBuilder sb = new StringBuilder();
		String del = "";
		int i = 0;
		for (String featureName : phraseFeatures.keySet()) {
			sb.append(del + featureName + "^" + phraseFeaturesWeights[i++]);
			del = " ";
		}
		return sb.toString();
	}

	private String constructQfString(double[] weightVector) {
		double[] termFeaturesWeights = getTermFeaturesWeights(weightVector);
		StringBuilder sb = new StringBuilder();
		String del = "";
		int i = 0;
		for (String featureName : termFeatures.keySet()) {
			sb.append(del + featureName + "^" + termFeaturesWeights[i++]);
			del = " ";
		}
		return sb.toString();
	}

	private double[] getPhraseFeaturesWeights(double[] weightVector) {
		 return Arrays.copyOfRange(weightVector, 1 + numericFeatures.size() + termFeatures.size(), 
				   1 + numericFeatures.size() + termFeatures.size() + phraseFeatures.size());
	}

	private double[] getTermFeaturesWeights(double[] weightVector) {
		return Arrays.copyOfRange(weightVector, 1 + numericFeatures.size(), 
				   1 + numericFeatures.size() + termFeatures.size());
	}

	private boolean isExperimentQuery(double[] candidateWeightVector) {
		return (candidateWeightVector != null) ? true : false;
	}

	private synchronized double[] tryUseSavedWeightVector() {
		if (lastSavedWeightVector != null) 
			return lastSavedWeightVector;
		return null;

	}

	private synchronized void cacheWeightVector(double[] currentWeightVector) {
		this.lastSavedWeightVector = currentWeightVector;		
	}

	/**
	 * fake scores
	 * @param list
	 * @return
	 */
	private DocList buildSingleDocList(int[] list) {
		float[] scores = new float[list.length];
			
		for (int i =0 ; i < list.length; i ++) 
			scores[i] = list.length - i;
		
		return new DocSlice(0, list.length, list, scores, list.length, scores[0]);
	}

	/**
	 * fake scores
	 * @param interleavedList
	 * @return
	 */
	private DocList buildInterleavedDocList(int[] interleavedList) {
		 
		float[] scores = new float[interleavedList.length];
		
		for (int i =0 ; i < interleavedList.length; i ++) 
			scores[i] = interleavedList.length - i;
		
		return new DocSlice(0, interleavedList.length, interleavedList, scores, interleavedList.length, scores[0]);
																													
	}
	

	/**
	 * Reranks the list returned using all the features
	 * @param docList
	 * @param searcher
	 * @param weightVector
	 * @param minFeatureScores
	 * @param maxFeatureScores
	 * @param allDocScores
	 * @param schema
	 * @param currentSecs
	 * @return
	 * @throws IOException
	 */
	private int[] searchReranking(DocList docList, SolrIndexSearcher searcher, double[] weightVector,
			Object[] minFeatureScores, Object[] maxFeatureScores, IndexSchema schema, long currentSecs) throws IOException {
		  		
		 DocIterator iterator = docList.iterator(); 
		 
		 Map <Integer, Float> docScores = new HashMap<Integer, Float>();
		 
		  while (iterator.hasNext()) {
			  int docId = iterator.nextDoc();

			  Document doc = searcher.doc(docId);
			  Float textScore = iterator.score();
			  
			  float normalizedTextScore = (textScore - (Float)minFeatureScores[0]) / ((Float)maxFeatureScores[0] - (Float)minFeatureScores[0]);
			 
			  float newScore = 0;
			  
			  //text similarity normalized score
			  newScore += normalizedTextScore * weightVector[0];											
					
			  int i = 1; //start from position 1 (because of overall text feature)
			  for (String numericFeatureName : numericFeatures.keySet()) {	//used TreeSet, so ordering is preserved
				  Object  maxValue, minValue;
				  float normalisedFeatureScore = 0;
				  IndexableField field = doc.getField(numericFeatureName);
				  if (field == null)	//ignore a field if it is null
					  continue;
				  Object docValue = schema.getFieldType(numericFeatureName).toObject(field); //get actual object
				  
				//compute normalized scores for each feature
				  	if (docValue instanceof Date) {	//handle date
					  	long secs = ((Date)docValue).getTime() / 1000;	//to seconds
					  	Long diff = currentSecs - secs;
					  	maxValue = (Long) maxFeatureScores[i];
						minValue = (Long) minFeatureScores[i];
						normalisedFeatureScore = (diff - (Long)minValue)  / (float)((Long)maxValue - (Long)minValue);
				  	}
				  
				  	else if (docValue instanceof Integer) {
						maxValue = (Integer) maxFeatureScores[i];
						minValue = (Integer) minFeatureScores[i];
						
						normalisedFeatureScore = ((Integer)docValue - (Integer)minValue)  / (float)((Integer)maxValue - (Integer)minValue);
					}
					else if (docValue instanceof Float) {
						maxValue = (Float) maxFeatureScores[i];
						minValue = (Float) minFeatureScores[i];
						normalisedFeatureScore = ((Float)docValue - (Float)minValue)  / ((Float)maxValue - (Float)minValue);
					}
					else if (docValue instanceof Double) {
						maxValue = (Double) maxFeatureScores[i];
						minValue = (Double) minFeatureScores[i];
						normalisedFeatureScore = (float) (((Double)docValue - (Double)minValue)  / ((Double)maxValue - (Double)minValue));
					}
					else if (docValue instanceof Long) {
						maxValue = (Double) maxFeatureScores[i];
						minValue = (Double) minFeatureScores[i];
						normalisedFeatureScore = ((Long)docValue - (Long)minValue)  / (float)((Long)maxValue - (Long)minValue);
					}
					newScore += normalisedFeatureScore * weightVector[i];
					
					i++;
			  	}
			  docScores.put(docId, newScore);
		  }
		 
		//sort docs
		SortedSet<Entry<Integer, Float>> sortedDocs = Utils.entriesSortedByValues(docScores) ;
		
		int resultLength = Math.min(defaultRows,sortedDocs.size());
		int[] docIds = new int[resultLength];
		int i=0;
		for (Entry<Integer,Float> entry : sortedDocs) {
			docIds[i++] =entry.getKey();
			if (i == resultLength)	//cut list
				break;
		}
		
		return docIds;
	}

	private void getMinMax(SolrIndexSearcher searcher, DocList docList, Object[] minFeatureScores,
			Object[] maxFeatureScores, IndexSchema schema, long currentSecs) throws IOException {
		  DocIterator iterator = docList.iterator();
			  
		  while (iterator.hasNext()) {
			  int docId = iterator.nextDoc();
			  Document doc = searcher.doc(docId);
			  Float score = iterator.score();
			  
			  //relevance score
			  if (minFeatureScores[0] == null) {	//first document
				  minFeatureScores[0] = score;
				  maxFeatureScores[0] = score;
			  }
			  else {
				  if (score >  (Float) maxFeatureScores[0])
					  maxFeatureScores[0] = score;
				  else if (score <  (Float) minFeatureScores[0])
					  minFeatureScores[0] = score;
			  }
			  //////////////////		  
			  
			  //numeric features score
			  int i = 1; //start from position 1 (because of overall text feature)
			  for (String numericFeatureName : numericFeatures.keySet()) {
				  
				  IndexableField field = doc.getField(numericFeatureName);
				  if (field == null)		//ignore docs that do not have a certain field
					  continue;
				  
				  Object obj = schema.getFieldType(numericFeatureName).toObject(field); //get actual object
				  
				  if (minFeatureScores[i] == null || maxFeatureScores[i] == null) {	//first document 
					  if (obj instanceof Date) {
						  Long secs = ((Date)obj).getTime() / 1000;	//to seconds
						  Long diff = currentSecs - secs;	//difference between query time and creation time
						  minFeatureScores[i] = diff;
						  maxFeatureScores[i] = diff;
					  }
					  else {
						  minFeatureScores[i] = obj;
						  maxFeatureScores[i] = obj;
					  }
				  }
				  
				  else {					  
					  if (obj instanceof Date) {	//handle date
						  	long secs = ((Date)obj).getTime() / 1000;	//to seconds
						  	Long diff = currentSecs - secs;
							if (diff < (Long) minFeatureScores[i] )
								minFeatureScores[i] = diff;
							else if (diff > (Long) maxFeatureScores[i])
								maxFeatureScores[i] = diff;
					  }
					  
					  else if (obj instanceof Long) {
	            			if ((Long)obj < (Long) minFeatureScores[i])
	            				minFeatureScores[i] = obj;
	            			else if ((Long)obj > (Long) maxFeatureScores[i])
	            				maxFeatureScores[i] = obj;
	            		}
	            		else if (obj instanceof Double) {
	            			if ((Double)obj < (Double) minFeatureScores[i])
	            				minFeatureScores[i] = obj;
	            			else if ((Double)obj > (Double) maxFeatureScores[i])
	            				maxFeatureScores[i] = obj;
	            		}
	            		else if (obj instanceof Integer) {
	            			if ((Integer)obj < (Integer) minFeatureScores[i])
	            				minFeatureScores[i] = obj;
	            			else if ((Integer)obj > (Integer) maxFeatureScores[i])
	            				maxFeatureScores[i] = obj;
	            		}
	            		else if (obj instanceof Float) {
	            			if ((Float)obj < (Float) minFeatureScores[i])
	            				minFeatureScores[i] = obj;
	            			else if ((Float)obj > (Float) maxFeatureScores[i])
	            				maxFeatureScores[i] = obj;
	            		}
	            	
				  }
				  i++;
			  }
			  //////////////////
		  }		
	}

		
	@Override
	  public NamedList getStatistics() {
		NamedList<Object> lst = new SimpleOrderedMap<Object>();
		int i = 0;
		
		lst.add("Overall text feature weight (last saved)", lastSavedWeightVector[i++]);
		
		if (!numericFeatures.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			String del = "";
			for (String name : numericFeatures.keySet()) {
				sb.append(del).append(name).append("=").append(lastSavedWeightVector[i++]);
				del = ", ";
			}
			sb.append("}");
			lst.add("Numeric Features weights (last saved)", sb.toString());
		}
		if (!termFeatures.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			String del = "";
			for (String name : termFeatures.keySet()) {
				sb.append(del).append(name).append("=").append(lastSavedWeightVector[i++]);
				del = ", ";
			}
			sb.append("}");
			lst.add("Term Features weights (last saved)", sb.toString());
		}
		if (!phraseFeatures.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			String del = "";
			for (String name : phraseFeatures.keySet()) {
				sb.append(del).append(name).append("=").append(lastSavedWeightVector[i++]);
				del = ", ";
			}
			sb.append("}");
			lst.add("Phrase Features weights (last saved)", sb.toString());
		}
		lst.add("Default rows",  defaultRows);
		lst.add("Internal result length", internalResultLength);

		return lst;

	  }
	
	  @Override
	  public  String getDescription() {
		  return "https://bitbucket.org/ilps/solr-online-learning-to-rank-plug-in";
	  }
	  @Override
	  public  String getSource() {
		  return "https://bitbucket.org/ilps/solr-online-learning-to-rank-plug-in";
	  }
	  
	  @Override
	  public String getVersion() {
	    return "Version 1.0";
	  }


}
