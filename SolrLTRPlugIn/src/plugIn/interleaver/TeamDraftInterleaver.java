package plugIn.interleaver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;


public class TeamDraftInterleaver implements Interleaver{

	
	@Override
	public Result interleave(int[] list1, int[] list2, int n) {
		
		int length = Math.min(n,Math.min(list1.length, list2.length));
		if (length == 0)
			return new Result(null, null);
		int i1 = 0;
		int i2 = 0;
		int i = 0;
		
		//list containing the  documents ids
		List<Integer> interleavedList = new ArrayList<Integer>();
		
		
		Set<Integer> selectedIds = new HashSet<Integer>();
		
		
		int[] assignments = new int[length];
			
		//determine overlap in top results
		for (int j = 0; j < length; j++) {

			
			int id1 = list1[j];
			int id2 = list2[j];

			if  (id1 == id2) {
				interleavedList.add(id1); 
				selectedIds.add(id1);
				assignments[i++] = -1;
				i1++;
				i2++;
			}
			else
				break;
		}
		
		int a1 = 0;
		int a2 = 0;
		while (interleavedList.size() < length) {
			if ((a1<a2) || ((a1==a2) && (randomInteger(0,1) == 0))) {
				assignments[i++] = 0;
				a1++;
				while (true) {
					//SolrDocument nextDoc = list1.get(i1);
					int nextDoc = list1[i1];
					
					//String id1 = (String) nextDoc.get("id");
					i1++;
					if (!selectedIds.contains(nextDoc)) {	//TODO: slow?
						selectedIds.add(nextDoc);	//TODO: check this
						interleavedList.add(nextDoc);
						break;
					}
				}
			}
			else {
				assignments[i++] = 1;
				a2++;
				while (true) {
					int nextDoc = list2[i2];
					i2++;
					if (!selectedIds.contains(nextDoc)) {
						selectedIds.add(nextDoc);	//TODO: check this
						interleavedList.add(nextDoc);
						break;
					}
				}
			}
		}
		
//		if ( n != i)	//Testing...
//			return null;
//		
//		SolrDocumentList interleavedList
//		int fakeScore = l.size();	//fake score
//		int j =0;
//		for (int docId : l)  
//			scoreDocs[j++] = new ScoreDoc(docId, fakeScore--);
	//	TopDocs interleavedList = new TopDocs(l.size(), scoreDocs, l.size()); 
		
		
		int [] resultList = new int[interleavedList.size()];
		Iterator<Integer> iter = interleavedList.iterator();
		int j = 0 ;
		while (iter.hasNext()) {
			int id = iter.next();
			resultList[j++] = id;
		}
			
		return new Result(resultList, assignments);
	}

	
	//TODO: REMOVE this from here. Interpret will happen in the common structure
	@Override
	public int interpret(int[] clickedDocs, int[] assignments,
			int[] interleavedList) {
			return 0;
	}

	
	int randomInteger(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}

}
