package plugIn.interleaver;

public interface  Interleaver {
	
	//the second list is with the candidate weight vector
	public Result interleave(int[] list1, int[] list2, int n);
	
	/**
	 * 
	 * @param clickedDocs
	 * @param assignments
	 * @param interleavedList
	 * @return -1 if the list with the new weight vector wins, 1 otherwise
	 */
	public int interpret(int[] clickedDocs, int[] assignments, int[] interleavedList);
	
	
	class Result {
		public int[] interleavedList;
		public int[] assignments = null;
		Result(int[] interleavedList, int[] assignments) {
			this.interleavedList = interleavedList;
			this.assignments = assignments;
		}
	}
}

