import cherrypy
import threading
from random import gauss
from numpy import array, zeros, ones
from numpy.linalg import norm
from sys import exit
import numpy as np
import time
import os
import shutil
import json
import urllib 
import ConfigParser

config = ConfigParser.RawConfigParser()
config.read('server_config.cfg')

#server holding common structure for solr learning query plug-in
#A requirement is to have the same 'k', which is the number of features for the weight vector 
class SolrPluginServer(object):
    
    def __init__(self):
        self.lock = threading.Lock()
        self.current_qid = -1 
        self.experimentPending = False
        self.clicked_docs = set()
        self.glbLock = threading.Lock()     #lock for each request. This might be a performance bottleneck
        self.interleaved_list = None
        self.assignments = None
        self.full_query = None
        self.original_list1 = None
        self.original_list2 = None

        self.old_weight_vector = None #not none if weight vector changed
        
        self.candidate_weight_vector = None

        self.readproperties()

        self.maxLogFileSize  = 2097152  #2 mb

        self.wvFilename = 't.wv'

        self.logFilenamePrefix = 'history.log'

        self.logFilenameSuffixNumber = 0
        
        self.logFile = open(self.logFilenamePrefix, 'w')
        self.logFile.close()

        #self.read_weight_vector()
        

    def readproperties(self):
        self.alpha = config.getfloat('server', 'alpha')
        self.delta = config.getfloat('server', 'delta')
        self.n = config.getint('server', 'n')

    def receivewv(self, wv):
        msg = ''
        self.glbLock.acquire()

        try:
            weights_list = [float(x) for x in wv.split(",")]
            self.init_weight_vector(weights_list)
            if self.k > 1:
                msg = 'OK'
            #at least 2 features should be learned
            else:
                print ('Exiting... At least 2 features should be learned.')
                exit(1)
        finally:
            #release the lock anyway
            self.glbLock.release()
        if msg == 'OK' :
            print 'Server READY..'
            print self.current_weight_vector
        return msg
    
    def searchrequest(self, qid, offset):  
        msg = ''
        #acquire the lock
        self.glbLock.acquire()
        try:

            #if it is a new experiment, update the current_qid
            #(offset == 0) handles pagination : we don't want a second page query to be an experimental query
            if self.experimentPending == False and int(offset) == 0:
                
                self.experimentPending = True
                self.current_qid = qid
                
                self.epsilon = self.sample_unit_sphere()
                self.candidate_weight_vector = self.generate_candidate_weight_vector()
                
                msg = np.array_str(self.current_weight_vector) + ';' + np.array_str(self.candidate_weight_vector) 
                print 'New experiment..'
                t = threading.Timer(self.n, self.interpret)
                t.start()
            else:
                 msg = np.array_str(self.current_weight_vector) 
        finally:
            #release the lock anyway
            self.glbLock.release()
                
        return msg
        
    def addclick(self, qid, docid):
        success = False
        
        #acquire the lock
        self.glbLock.acquire()
        try:
            if qid == self.current_qid and (docid not in self.clicked_docs) and self.interleaved_list != None and self.assignments != None:
                self.clicked_docs.add(docid)
                success = True
        finally:
            #release the lock anyway
            self.glbLock.release()
        
        if success:
            print 'Added click with docid = ' +docid
            return 'OK'
        else:
            print 'Ignored click with docid = ' +docid
            return 'IGNORED'
            
    def getclicks(self, qid):
        success = False
        
        #acquire the lock
        self.glbLock.acquire()
        try:
            if qid == self.current_qid:
                success = True
        finally:
            self.glbLock.release()
            
        if success:       
            return ','.join(str(e) for e in self.clicked_docs)
        else:
            return 'Error'
    
    #l : interleaved list
    #a : assignments
    def postlists(self, qid, il, a, fq, l1, l2):
        success = False
        
        #acquire the lock
        self.glbLock.acquire()
        try:
            if self.experimentPending and qid == self.current_qid:
                success = True
                self.interleaved_list = [x for x in il.split(",")]   
                self.assignments = [int(x) for x in a.split(",")]
                self.full_query = fq
                self.original_list1 = [x for x in l1.split(",")]
                self.original_list2 = [x for x in l2.split(",")]
                print "Received lists"
        finally:
            self.glbLock.release()
            
        if success:       
            return 'OK'
        else:
            return 'ERROR'
    
    #called after an experiment is ended    
    def clear_structures(self):
        self.current_qid = -1 
        self.experimentPending = False
        self.clicked_docs = set()
        self.interleaved_list = None
        self.assignments = None
        self.candidate_weight_vector = None
        self.full_query = None
        self.original_list1 = None
        self.original_list2 = None
        self.old_weight_vector = None #not none if weight vector changed
        
    #Implements teamdraft  
    def interpret(self):
        print 'interleaved list: ' + str(self.interleaved_list)
        print 'assignments: ' + str(self.assignments)
        print 'clicked docs: ' + str(self.clicked_docs)
        
        #acquire the lock
        self.glbLock.acquire()
        try:
            a1 = 0
            a2 = 0
            if self.interleaved_list == None or self.assignments == None or len(self.interleaved_list) != len(self.assignments):
                print 'ERROR with interleaved and assignment lists'
        
            else:
                for i in range(0,len(self.interleaved_list)):
                    docid = self.interleaved_list[i]
                    if docid in self.clicked_docs:
                        a = self.assignments[i]
                        if a == 0:
                            a1 += 1
                        elif a == 1:
                            a2 += 1
                            
                            
                if a2 > a1:     #candidate vector wins
                    self.update_weight_vector()
                    print 'Candidate vector wins'
                else:
                    print 'Current vector wins'
                
        finally:
            #log the experiment
            self.log_experiment()

            self.clear_structures()
            self.glbLock.release()


    def log_experiment(self) :
        #create json with the following
        d = dict()
        d['qid'] = self.current_qid
        d['clicked_docs'] = list(self.clicked_docs)
        d['interleaved_list'] = self.interleaved_list
        d['assignments'] = self.assignments
        d['query'] = urllib.unquote(self.full_query).decode('utf8') 
        d['l1'] = self.original_list1
        d['l2'] = self.original_list2
        d['n'] = self.n
        d['timestamp'] = time.time()
        wv = None
        new_wv = None
        if (self.old_weight_vector != None): #weight vector changed 
            wv = self.old_weight_vector
            new_wv = self.current_weight_vector
        else:
            wv = self.current_weight_vector

        d['wv'] = list(wv)
        if (new_wv != None):
            d['new_wv'] = list(new_wv) #will be none if not changed
        else   :
            d['new_wv'] = None

        #print d
        #convert to json
        d_json = json.dumps(d, ensure_ascii=False)

        #write to file
        self.logFile = open(self.logFilenamePrefix, 'a') 

        self.logFile.write(d_json+'\n')

        self.logFile.close()

        #if log file bigger than self.maxLogFileSize, backup and create a new one
        if (os.path.getsize(self.logFilenamePrefix) > self.maxLogFileSize ):
            self.logFilenameSuffixNumber+=1
            self.backupLog()
            self.logFile = open(self.logFilenamePrefix, 'w')   
            self.logFile.close()

            

    def backupLog(self) :
        shutil.copy(self.logFilenamePrefix, self.logFilenamePrefix + '.'+str(self.logFilenameSuffixNumber))
       
    def update_weight_vector(self):
        self.old_weight_vector = self.current_weight_vector #for logging
        for i in range(0, len(self.current_weight_vector)):
            self.current_weight_vector[i] += self.alpha * self.epsilon[i]
        self.candidate_weight_vector = None
        self.write_weight_vector()
        
    def init_weight_vector(self, weights_list):
        self.k = len(weights_list)
        self.current_weight_vector = zeros(self.k)

        #if weight vector does not exist (first run), initialize with the values from the solr plug-in
        if not os.path.isfile(self.wvFilename):
            for i in range(0, self.k):
                self.current_weight_vector[i] = weights_list[i]
            self.write_weight_vector()
        else:
            with open(self.wvFilename, 'r') as f:
                l = f.readline().split()
            #if the length of the saved weight vector agrees with the length of the solr plug-in weight vector,
            if len(l) == self.k :
                for i in range(0, len(l)):
                    self.current_weight_vector[i] = float(l[i])
            else:
                for i in range(0, self.k):
                    self.current_weight_vector[i] = weights_list[i]
                self.write_weight_vector();
           
    def write_weight_vector(self):
        s = ''
        d = ''
        for i in self.current_weight_vector:
            s+= d+str(i)
            d=' '
        with open(self.wvFilename, 'w') as f:
            f.write(s)
        
    def normalize_to_unit_sphere(self, v):
        return v / norm(v)

    def sample_unit_sphere(self):
        """See http://mathoverflow.net/questions/24688/efficiently-sampling-
        points-uniformly-from-the-surface-of-an-n-sphere"""
        v = zeros(self.k)
        for i in range(0, self.k):
            v[i] = gauss(0, 1)
        return self.normalize_to_unit_sphere(v)
        
    def generate_candidate_weight_vector(self):

        v = zeros(self.k)
        for i in range(0, self.k):
            v[i] = self.current_weight_vector[i] + self.delta * self.epsilon[i]
        return v
        
    def currentweightvector(self):       
        if self.current_weight_vector == None:
            return 'None'
        return np.array_str(self.current_weight_vector)
        
    searchrequest.exposed = True
    addclick.exposed = True
    getclicks.exposed = True    #TESTING
    postlists.exposed = True 
    interpret.exposed = True
    receivewv.exposed = True
    currentweightvector.exposed=True

port = config.getint('server', 'port')
host = config.get('server', 'host')

cherrypy.config.update({'server.socket_host': host, \
                         'server.socket_port': port, \
                        }) 

cherrypy.quickstart(SolrPluginServer())